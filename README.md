# OpenML dataset: Google-Play-Store-Reviews

https://www.openml.org/d/43565

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Google Play Store
Google Play, formerly Android Market, is a digital distribution service operated and developed by Google. It serves as the official app store for certified devices running on the Android operating system, allowing users to browse and download applications developed with the Android software development kit (SDK) and published through Google. It has crossed over 82 billion app downloads with over 3.5 million published apps making it the largest app store in the world. 

Content
The data contains over 12000 reviews of different app store applications by real users. The data also contains the rating that was given by them so it can be classified into positive or negative reviews. This is a real good place to perform sentiment analysis tasks. Some of the apps whose data has been collected can be seen below. 

Acknowledgements
I have used the Google Play Store Scrapper Library for this task which made the scrapping really easy. You can also check out the data collection process here. 
Motivation
This is a really good place to start learning about sentiment analysis. These reviews and ratings can provide beginners with annotated real-world data. I have uploaded a BERT sentiment analysis model which has been trained on Colab but the code can be reused to run on Kaggle as well. You can also mine some other insights regarding the best and the worst reviews.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43565) of an [OpenML dataset](https://www.openml.org/d/43565). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43565/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43565/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43565/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

